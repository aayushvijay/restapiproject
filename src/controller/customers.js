const customer = require('../models/customers');
const validation = require('../utils/validation.js');

const readCustomerData = function(req, res, next) {
  customer
    .readCustomerData()
    .then(allValues => {
      if (allValues.length !== 0) {
        res.send(allValues);
      } else {
        res.status(404);
        next({
          message: 'No customers data available',
          statusCode: 404
        });
      }
    })
    .catch(err => {
      res.status(400).send(err);
      next({ message: 'Bad Request', statusCode: 400 });
    });
};
const readCustomerWithId = function(req, res, next) {
  const request = validation.schema1.validate(req.params);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else {
    customer
      .readCustomerDataWithId(req.params.id)
      .then(value => {
        if (value.length !== 0) {
          res.status(200).send(value);
        } else {
          res.status(404);
          next({
            message: `Customer with Id ${req.params.id} not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
const insertNewCustomer = function(req, res, next) {
  const request = validation.schema2.validate(req.body);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else {
    const insertNewCustomer = Object.values(req.body);
    customer
      .insertCustomerInCustomersTable(insertNewCustomer)
      .then(() => res.status(200).send(`New customer inserted successfully`))
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
const updateCustomerWithId = function(req, res, next) {
  const requestId = validation.schema1.validate(req.params);
  const requestBody = validation.schema2.validate(req.body);
  if (requestId.error) {
    res.status(400).send(requestId.error.details[0].message);
    next(requestId.error.details[0].message);
  } else if (requestBody.error) {
    res.status(400).send(requestBody.error.details[0].message);
    next(requestBody.error.details[0].message);
  } else {
    const updateCustomer = Object.values(req.body);
    updateCustomer.push(parseInt(req.params.id));
    customer
      .updateCustomerInCustomersTable(updateCustomer)
      .then(data => {
        if (data.rowCount === 0) {
          res.status(404).send(`Customer id ${req.params.id} not available`);
        } else {
          res.status(200).send(`Customer with id ${req.params.id} updated`);
        }
      })
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
const deleteCustomerWithId = function(req, res, next) {
  const request = validation.schema1.validate(req.params);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else {
    customer
      .deleteCustomerWithId(req.params.id)
      .then(data => {
        if (data.rowCount === 0) {
          res
            .status(404)
            .send(`Customer with id ${req.params.id} not available`);
        } else {
          res.status(200).send(`Customer with id ${req.params.id} deleted`);
        }
      })
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
module.exports = {
  readCustomerData,
  readCustomerWithId,
  insertNewCustomer,
  updateCustomerWithId,
  deleteCustomerWithId
};