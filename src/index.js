const customer = require('./routes/customers.js');
const order = require('./routes/orders.js');
const logger = require('./middleware/logging.js');
const errorHandler = require('./middleware/errorHandler.js');
const express = require('express');
const app = express();

app.use(express.json());
app.use(logger.log);
app.get(
  '/',
  (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' });
  },
  errorHandler
);
app.use('/customer', customer, errorHandler);
app.use('/order', order, errorHandler);
app.use(
  '*',
  function(req, res) {
    res.status(404).send(`Invalid Url - 404 Page`);
  },
  errorHandler
);

app.use(logger.error);

const port = process.env.Port || 3000;
app.listen(port, function(err) {
  if (err) {
    console.log('error while starting server');
  } else {
    console.log(`Listening at port ${port}`);
  }
});
