const { Pool } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'aman_vj0896',
  port: 5432
});
async function readOrdersData() {
  try {
    let ordersData = await pool.query('SELECT * FROM orders');
    return ordersData.rows;
  } catch (err) {
    return err;
  }
}
async function readOrderDataWithId(id) {
  try {
    let orderWithId = await pool.query(
      'SELECT * FROM orders WHERE order_id = $1',
      [id]
    );
    return orderWithId.rows;
  } catch (err) {
    return err;
  }
}
async function readOrderDataWithCustomerId(customerId) {
  try {
    let orderWithItemQuantityAndAmount = await pool.query(
      `SELECT customers.customer_id, customer_name, COUNT(item_name) item_count, SUM(item_amount) total_amount 
      FROM customers INNER JOIN orders ON customers.customer_id = orders.customer_id 
      WHERE customers.customer_id=$1 
      GROUP BY customers.customer_id;`,
      [customerId]
    );
    return orderWithItemQuantityAndAmount.rows;
  } catch (err) {
    return err;
  }
}
async function insertOrderInOrdersTable(array) {
  try {
    return await pool.query(
      `INSERT INTO orders(order_id, customer_id, item_name, item_amount) 
    VALUES((SELECT MAX(order_id) FROM orders)+1, $1, $2, $3)`,
      array
    );
  } catch (err) {
    return err;
  }
}
async function updateOrderInOrdersTable(array) {
  try {
    return await pool.query(
      `UPDATE orders SET customer_id = $1, item_name = $2, item_amount = $3 WHERE order_id = $4`,
      array
    );
  } catch (err) {
    return err;
  }
}
async function deleteOrderWithId(orderId) {
  try {
    return await pool.query(`DELETE FROM orders WHERE order_id = $1`, [
      orderId
    ]);
  } catch (err) {
    return err;
  }
}
module.exports = {
  readOrdersData,
  readOrderDataWithId,
  readOrderDataWithCustomerId,
  insertOrderInOrdersTable,
  updateOrderInOrdersTable,
  deleteOrderWithId
};
